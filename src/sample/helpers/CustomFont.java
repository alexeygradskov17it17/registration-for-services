package sample.helpers;

import javafx.scene.text.Font;


public class CustomFont {
    static Font font;
    static String fontPath = "file:src/sample/fonts/19956.otf";
    static int size = 14;


     private CustomFont(){}

     public static Font getFont(){
         if (font==null){
             font = Font.loadFont(fontPath,size);
         }
         return font;
     }


}
