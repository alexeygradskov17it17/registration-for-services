package sample;

public class Client {
    int id, genderCode;
    String firstName, lastName, patronymic, email, phone, photoPath, birthday, registrationDate;

    public Client(int id, String firstName,
                  String lastName, String patronymic,
                  String birthday, String registrationDate,
                  String email, String phone,
                  int genderCode,String photoPath){
        this.id = id;
        this.firstName=firstName;
        this.lastName=lastName;
        this.patronymic=patronymic;
        this.birthday=birthday;
        this.registrationDate=registrationDate;
        this.email=email;
        this.phone=phone;
        this.genderCode=genderCode;
        this.photoPath=photoPath;
    }
}
