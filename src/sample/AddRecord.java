package sample;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import sample.helpers.CustomFont;

import java.sql.SQLException;
import java.sql.Time;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AddRecord {
    public ChoiceBox<String> clientCB;
    public Button addBtn;
    public Label clientLabel;
    public Label serviceLabel;
    public Label labelDate;
    public Label labelDescription;
    String clientValue;
    public ChoiceBox<String> serviceCB;
    public TextField dateField;
    public TextArea commentField;
    BaseData baseData = BaseData.getBaseData();


    public void initialize() throws SQLException {
        List<String> names = getIdWithName(baseData.selectClients("*", ""));
        ObservableList<String> clientsName = FXCollections.observableArrayList(names);
        String[] predicates = {"discount=discount", "Title=Title"};
        List<String> titles = getIdWithTitle(baseData.select("*", predicates, ""));
        ObservableList<String> serviceTitles = FXCollections.observableArrayList(titles);
        serviceCB.setItems(serviceTitles);
        clientLabel.setFont(CustomFont.getFont());
        serviceLabel.setFont(CustomFont.getFont());
        labelDate.setFont(CustomFont.getFont());
        labelDescription.setFont(CustomFont.getFont());
        addBtn.setFont(CustomFont.getFont());

        addBtn.setBackground(new Background(new BackgroundFill(Color.rgb(255, 156, 26),
                CornerRadii.EMPTY,
                Insets.EMPTY)));
        serviceCB.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                serviceCB.setValue(serviceCB.getValue());
            }
        });
        clientCB.setItems(clientsName);
        clientCB.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                clientValue = clientCB.getValue();
            }
        });
    }

    public ArrayList<String> getIdWithName(ArrayList<Client> arrayList) {
        ArrayList<String> names = new ArrayList<>();
        for (Client client : arrayList) {
            names.add(client.id + " " + client.firstName + " " + client.lastName + " " + client.patronymic);
        }
        return names;
    }

    public ArrayList<String> getIdWithTitle(ArrayList<Service> arrayList) {
        ArrayList<String> titles = new ArrayList<>();
        for (Service service : arrayList) {
            titles.add(service.id + " " + service.title);
        }
        return titles;
    }

    public void addNewRecord(ActionEvent actionEvent) throws SQLException {
        Record record = new Record(0, 0, 0, "", "");
        LocalDateTime localDateTime = LocalDateTime.parse(dateField.getText());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        record.datetime = dateField.getText();
        if (!(clientCB.getValue() == null)
                && !(serviceCB == null)
                && !commentField.getText().equals("")
                && !dateField.getText().equals("")) {
            Pattern pattern = Pattern.compile("^\\d*");
            Matcher matcher = pattern.matcher(serviceCB.getValue());
            while (matcher.find()) {
                record.serviceID = Integer.parseInt(matcher.group());

            }
            ;
            matcher = pattern.matcher(clientCB.getValue());
            while (matcher.find()) {
                record.clientID = Integer.parseInt(matcher.group());
            }
            record.comment = commentField.getText();
            baseData.insertRecord(record);
            Stage stage = (Stage) commentField.getScene().getWindow();
            stage.close();
        }
        ;
    }
}
