package sample;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import sample.Service;
import sample.helpers.CustomFont;

import java.awt.event.KeyListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class Controller {
    public TextField findField;
    public ChoiceBox<String> orderByCB;
    public Label quantityOfServices;
    @FXML
    private ChoiceBox<String> sizesOfDiscount;
    @FXML
    AnchorPane window;
    @FXML
    private ListView<Service> listView;

    @FXML
    private Label contactLabel;


    @FXML
    private Label numberOfTelephone;

    List<Service> services;
    String[] predicates = {"discount=discount", "Title=Title"};
    BaseData baseData = BaseData.getBaseData();
    String value = "все";
    Timer timer = new Timer();
    ObservableList<String> discounts = FXCollections.observableArrayList(
            "все",
            "от 0 до 5%",
            "от 5% до 15%",
            "от 15% до 30%",
            "от 30% до 70%",
            "от 70% до 100%");
    ObservableList<String> strings = FXCollections.observableArrayList("по цене(низ)", "по цене(выс)");
    String orderBy = "";
    int quantityOfAllServices;
    int quantityOfServicesNow;
    {
        try {
             quantityOfAllServices = baseData.select("*", predicates, orderBy).size();

        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    public void initialize() throws SQLException {
        findField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (timer != null) {
                timer.cancel();
                timer = new Timer();
            }
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    Platform.runLater(new Runnable() {
                        public void run() {
                            find(newValue);
                        }
                    });

                }
            }, 500);
        });

        sizesOfDiscount.setValue(value);
        services = baseData.select("*", predicates, orderBy);
        quantityOfServicesNow = services.size();
        quantityOfServices.setFont(CustomFont.getFont());
        quantityOfServices.setText(quantityOfServicesNow+" записей из "+quantityOfAllServices);
        contactLabel.setStyle("-fx-font-weight: bold");
        contactLabel.setFont(CustomFont.getFont());
        numberOfTelephone.setFont(CustomFont.getFont());
        ObservableList<Service> servicesObservableList = FXCollections.observableArrayList(services);
        listView.setItems(servicesObservableList);
        listView.setCellFactory(studentListView -> new ListViewServiceCell());
        orderByCB.setItems(strings);
        orderByCB.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                orderByCB.setValue(orderByCB.getValue());
                switch (orderByCB.getValue()) {
                    case "по цене(низ)":
                        orderBy = " ORDER BY Cost ASC";
                        try {
                            initialize();
                        } catch (SQLException exception) {
                            exception.printStackTrace();
                        }
                        break;
                    case "по цене(выс)":
                        orderBy = " ORDER BY Cost DESC";
                        try {
                            initialize();
                        } catch (SQLException exception) {
                            exception.printStackTrace();
                        }
                        break;
                }
            }
        });
        sizesOfDiscount.setItems(discounts);
        sizesOfDiscount.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                sizesOfDiscount.setValue(sizesOfDiscount.getValue());
                switch (sizesOfDiscount.getValue()) {
                    case "все":
                        predicates[0] = "discount=discount";
                        value = "все";
                        try {
                            initialize();
                        } catch (SQLException exception) {
                            exception.printStackTrace();
                        }
                        break;
                    case "от 0 до 5%":
                        predicates[0] = "discount>=0 AND discount<5";
                        value = "от 0 до 5%";
                        try {
                            initialize();
                        } catch (SQLException exception) {
                            exception.printStackTrace();
                        }
                        break;
                    case "от 5% до 15%":
                        predicates[0] = "discount>=5 AND discount<15";
                        value = "от 5% до 15%";
                        try {
                            initialize();
                        } catch (SQLException exception) {
                            exception.printStackTrace();
                        }
                        break;
                    case "от 15% до 30%":
                        predicates[0] = " discount>=15 AND discount<30";
                        value = "от 15% до 30%";
                        try {
                            initialize();
                        } catch (SQLException exception) {
                            exception.printStackTrace();
                        }
                        break;
                    case "от 30% до 70%":
                        predicates[0] = "discount>=30 AND discount<70";
                        value = "от 30% до 70%";
                        try {
                            initialize();
                        } catch (SQLException exception) {
                            exception.printStackTrace();
                        }
                        break;
                    case "от 70% до 100%":
                        predicates[0] = "discount>=70 AND discount<100";
                        value = "от 70% до 100%";
                        try {
                            initialize();
                        } catch (SQLException exception) {
                            exception.printStackTrace();
                        }
                        break;
                }
            }
        });
    }

    public void find(String newValue) {
        if (!newValue.equals("".trim())) {
            predicates[1] = "Title LIKE '" + newValue.toLowerCase().trim() + "%'";
        } else predicates[1] = "Title=Title";
        try {
            initialize();
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }
}
